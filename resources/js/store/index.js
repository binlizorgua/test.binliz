import Vue from 'vue';
import Vuex from 'vuex';
import messages from './modules/messages/index';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {},
    getters: {},
    mutations: {},
    actions: {},
    modules: {
        messages
    },
});

