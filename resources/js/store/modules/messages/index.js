import Axios from 'axios';

const state = {
    messages: null,
    item: null,
    pagination: {
        currentPage: null,
        lastPage: null,
        total: null,
        from: null,
        to: null,
        path: null
    },
    page: 1,
    errors: null
};

const getters = {
    MESSAGES: state => {
        return state.messages;
    },
    MESSAGES_PAGES: state =>{
        return state.pagination;
    },
    MESSAGES_ITEM: state => {
        return state.item;
    },
    MESSAGES_PAGE: state =>{
        return state.page
    },

};


const mutations = {
    SET_MESSAGES: (state, payload) => {
        state.messages = payload;
    },
    SET_MESSAGES_ITEM: (state, payload) => {
        state.item = payload;
    },
    SET_MESSAGES_PAGINATION: (state, payload) => {
        state.pagination = payload;
    },
    SET_MESSAGES_PAGE: (state, payload) => {
        state.page = payload;
    },
};
const actions = {
    CREATE_MESSAGE: async (context, payload) => {
        await Axios.put(`/api/chat/postMessage`,payload).then(data => {
        });
    },
    SET_PAGE: async (context, payload) => {
        context.commit('SET_MESSAGES_PAGE', payload);
        context.dispatch('GET_MESSAGES');
    },
    GET_MESSAGES: async (context, payload) => {
        let $url = `/api/chat/getMessages`
        let page = context.getters.MESSAGES_PAGE;
        if(page > 1)
            $url += `?page=${page}`;
        let {data} = await Axios.get($url);
        context.commit('SET_MESSAGES', data.data);
        context.commit('SET_MESSAGES_PAGINATION',{
            currentPage: data.current_page,
            lastPage: data.last_page,
            total: data.total,
            from: data.from,
            to: data.to,
            path: data.path
        });
    },
};
export default {
    state,
    getters,
    mutations,
    actions,
};
