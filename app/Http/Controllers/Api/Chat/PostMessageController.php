<?php

namespace App\Http\Controllers\Api\Chat;

use App\Events\MessagePushed;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PostMessageController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        //
        $user = $request->user();

        $message = $user->messages()->create([
            'message' => $request->input('message')
        ]);
        broadcast(new MessagePushed($message));

        return response()->json(['status'=>'api:created']);

    }
}
