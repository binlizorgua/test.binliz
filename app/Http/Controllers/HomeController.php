<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Api\Chat\GetMessagesController;
use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }


    /**
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function chat()
    {
        return view('chat',['user'=>Auth::user()->id]);
    }

}
